package com.lionsquare.apod.model

class DateBean {

    var dateView: String? = null
    var dateSend: String? = null

    constructor(dateView: String?, dateSend: String?) {
        this.dateView = dateView
        this.dateSend = dateSend
    }

}