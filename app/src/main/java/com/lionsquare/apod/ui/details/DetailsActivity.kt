package com.lionsquare.apod.ui.details

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.lionsquare.apod.R
import com.lionsquare.apod.api.ApiResponse
import com.lionsquare.apod.databinding.ActivityDetailsBinding
import com.lionsquare.apod.model.Global
import com.lionsquare.apod.ui.BaseActivity
import com.lionsquare.apod.ui.main.MainViewModel

class DetailsActivity : BaseActivity() {

    private lateinit var detailsBinding: ActivityDetailsBinding
    private var detailsViewModel: DetailsViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        detailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        detailsViewModel = ViewModelProviders.of(this).get(DetailsViewModel::class.java)
        detailsBinding?.detailsViewModel = detailsViewModel
        detailsBinding.lifecycleOwner = this

        if (intent.extras != null) {
            val date = intent.extras!!.getString("date")
            initSetUp()
            detailsViewModel?.getApod(date!!)
        }

    }


    fun initSetUp() {
        detailsViewModel?.getApiRespons()
            ?.observe(this, Observer<ApiResponse> { apiRespones: ApiResponse? ->
                detailsBinding.immProgressConnexion.visibility = View.GONE
                if (apiRespones != null) {
                    //error
                }

                if (apiRespones?.apod != null) {
                    detailsBinding.tvTitle.text = apiRespones?.apod?.title
                    detailsBinding.tvExplanation.text = apiRespones?.apod?.explanation
                    detailsBinding.immProgress.visibility = View.VISIBLE
                    Glide.with(mContext).load(apiRespones?.apod?.url).listener(object :
                        RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            detailsBinding.immProgress.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {

                            detailsBinding.immProgress.visibility = View.GONE
                            return false
                        }
                    }).into(detailsBinding.ivApod)
                }

            })
    }
}
