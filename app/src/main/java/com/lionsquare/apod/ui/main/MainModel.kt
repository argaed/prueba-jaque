package com.lionsquare.apod.ui.main

import android.annotation.SuppressLint
import com.lionsquare.apod.model.DateBean
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class MainModel {

    @SuppressLint("SimpleDateFormat")
    fun getDates(): List<DateBean> {

        val dates = ArrayList<DateBean>()
        var date1: Date? = null
        var date2: Date? = null

        val dtStartDate = Date()
        val c = Calendar.getInstance()
        c.time = dtStartDate
        c.add(Calendar.DATE, -100) // number of days to add


        try {
            date1 = c.time
            date2 = Date()
        } catch (e: ParseException) {

        }

        val cal1 = Calendar.getInstance()
        cal1.time = date1
        val cal2 = Calendar.getInstance()
        cal2.time = date2

        val formatView = SimpleDateFormat("MMM dd, yyyy")
        val formatSend = SimpleDateFormat("yyyy-MM-dd")
        while (!cal1.after(cal2)) {

            dates.add(DateBean(formatView.format(cal1.time), formatSend.format(cal1.time)))
            cal1.add(Calendar.DATE, 1)
        }

        dates.reverse()
        return dates
    }
}