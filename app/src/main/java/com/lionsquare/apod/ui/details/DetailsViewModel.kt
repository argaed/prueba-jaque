package com.lionsquare.apod.ui.details

import android.app.Application
import android.provider.Settings
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.lionsquare.apod.api.ApiResponse
import com.lionsquare.apod.api.model.ApiRepo
import com.lionsquare.apod.model.Global

class DetailsViewModel(application: Application) :AndroidViewModel(application) {

    var apiResponse: MutableLiveData<ApiResponse>? = null


    fun getApiRespons(): MutableLiveData<ApiResponse> {
        if (apiResponse == null) {
            apiResponse = MutableLiveData()
        }
        return apiResponse!!
    }

    fun getApod(date: String) {
        ApiRepo().getApod(date,  getApiRespons())

    }
}