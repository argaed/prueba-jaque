package com.lionsquare.apod.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lionsquare.apod.R
import com.lionsquare.apod.adapter.DateAdapter
import com.lionsquare.apod.databinding.ActivityMainBinding
import com.lionsquare.apod.ui.BaseActivity
import com.lionsquare.apod.ui.details.DetailsActivity


class MainActivity : BaseActivity(), DateAdapter.Clicklistener {

    private var mainViewModel: MainViewModel? = null
    private lateinit var mainBinding: ActivityMainBinding

    private var rvDates: RecyclerView? = null
    private var dateAdapter: DateAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mainBinding.viewmodel = mainViewModel
        mainBinding.lifecycleOwner = this

        initRv()

    }

    fun initRv() {
        rvDates = mainBinding.rvDate
        rvDates!!.layoutManager = LinearLayoutManager(mContext)
        dateAdapter = DateAdapter(mainViewModel?.mainModel!!.getDates())
        dateAdapter?.setOnClickListener(this)
        rvDates!!.adapter = dateAdapter
    }

    override fun OnClick(date: String) {
        val i = Intent(mContext, DetailsActivity::class.java)
        i.putExtra("date", date)
        startActivity(i)
    }


}
