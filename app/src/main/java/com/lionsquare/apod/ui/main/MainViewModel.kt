package com.lionsquare.apod.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.lionsquare.apod.api.ApiResponse
import com.lionsquare.apod.api.model.ApiRepo
import com.lionsquare.apod.api.model.Apod

open class MainViewModel(application: Application) : AndroidViewModel(application) {

     val mainModel: MainModel? by lazy { MainModel() }


}