package com.lionsquare.apod.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiService {

    companion object {
        private const val urlBase="https://api.nasa.gov/"
        private var serviceApi: ApiEndpoints? = null

        fun getService(): ApiEndpoints {
            if (serviceApi == null) {
                val httpBuilder = OkHttpClient.Builder()
                val interceptor = HttpLoggingInterceptor()

                val client = httpBuilder
                    .addNetworkInterceptor(interceptor)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .addInterceptor(interceptor)
                    .build()
                interceptor.level = HttpLoggingInterceptor.Level.BODY

                val retrofit = Retrofit.Builder()
                    .baseUrl(urlBase)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())

                    .build()
                serviceApi = retrofit.create(ApiEndpoints::class.java)

            }

            return serviceApi!!
        }
    }


}