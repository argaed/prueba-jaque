package com.lionsquare.apod.api.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.lionsquare.apod.api.ApiEndpoints
import com.lionsquare.apod.api.ApiResponse
import com.lionsquare.apod.api.ApiService
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiRepo {
    var endpoints: ApiEndpoints = ApiService.getService()


    fun getApod(  date:String, apiResponse: MutableLiveData<ApiResponse>) {

        val call = endpoints.getApod( date)
        call.enqueue(object : Callback<Apod> {
            override fun onResponse(call: Call<Apod>, response: Response<Apod>) {
                if (response.isSuccessful) {
                    apiResponse.value = ApiResponse(response.body()!!)

                } else {
                    apiResponse.value = null
                }


            }

            override fun onFailure(call: Call<Apod>, t: Throwable) {
                Log.e("resp", t.toString())
                apiResponse.value = ApiResponse(t)
            }
        })

    }
}