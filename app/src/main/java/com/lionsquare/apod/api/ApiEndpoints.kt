package com.lionsquare.apod.api


import com.lionsquare.apod.api.model.Apod
import retrofit2.Call
import retrofit2.http.*

interface ApiEndpoints {

    //
    @GET("planetary/apod?api_key=6YJDFfM94Sdk8ULWqEsvuPWB238EzJy4XRWdsc0P")
    fun getApod(@Query("date") date: String): Call<Apod>


}