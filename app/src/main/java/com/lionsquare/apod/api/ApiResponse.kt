package com.lionsquare.apod.api

import com.lionsquare.apod.api.model.Apod


class ApiResponse {

    var error: Throwable? = null
    var apod: Apod? = null

    constructor(apod: Apod) {
        this.error = null
        this.apod = apod
    }


    constructor(error: Throwable) {
        this.error = error
    }
}
