package com.lionsquare.apod.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lionsquare.apod.R
import com.lionsquare.apod.model.DateBean

class DateAdapter(
    private var dateArray: List<DateBean>
) : RecyclerView.Adapter<DateAdapter.ViewHolder>() {
    private lateinit var clickListener: Clicklistener

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val view = inflater.inflate(R.layout.item_date, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dateItem = dateArray[position]
        holder.immFmRoot.setOnClickListener {
            clickListener.let {
                dateItem.dateSend?.let { item -> it.OnClick(item) }
            }
        }
        holder.tvDate.text = dateItem.dateView

    }

    override fun getItemCount(): Int {
        return dateArray.size
    }


    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val immFmRoot: LinearLayout = itemView.findViewById(R.id.ll_content)
        val tvDate: TextView = itemView.findViewById(R.id.txtDate) as TextView
    }

    fun setOnClickListener(clickListener: Clicklistener) {
        this.clickListener = clickListener
    }

    interface Clicklistener {
        fun OnClick(date: String)
    }
}